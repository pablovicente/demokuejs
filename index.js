var express = require('express');
var app = express();
var casual = require('casual');
var kue = require('kue');
var jobs = kue.createQueue();

var blocked = require('blocked');


// Este codigo es para simular un proceso muy pesado, en este caso el envio de un email
// por el cual probablemente esperariamos, aunque asincronicamente respuesta el servidor de mail.
function mockSendEmail(callback){
    for(var i=0;i<100;i++){
        Array(10000000).join('a');
    }
    callback();
};

// Simula un proceso largo en una ruta de express.
app.get('/slow', function (req, res) {
    mockSendEmail(function cb(){
            res.status(200).send('Email Sent!');
    });
});

app.get('/fast', function(req, res){
    var job  = jobs.create( 'Emails', casual.text );
    job.save(function(err){
       res.status(201).send('Email Queued');
    });
});

app.listen(3000, function () {
    console.log('Express server listening on port: 3000');
});


blocked(function(ms){
    console.log('BLOCKED FOR %sms', ms | 0);
});